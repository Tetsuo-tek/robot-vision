#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

###############################################################################

import sys
import argparse
import time

import torch

import cv2          as cv
import numpy        as np

from transformers   import AutoModelForCausalLM, AutoTokenizer, TextIteratorStreamer, StoppingCriteria, StoppingCriteriaList
from threading      import Thread, Lock

###############################################################################
# SKETCH

sat = FlowSat()
t = 0.1

thread = None
streamer = None
streamerEx = Lock()
active = False

promptChanName = "Vision.Prompt"
promptChan = None
promptPack = None
currentPrompt = None
lastCmdHash = None

inputChanName = "Vision.JPeg"
inputChan = None

frame = None

outputChanName = "Vision.TXT"
outputChan = None

closing = False

def setup():
    global args

    print("[SETUP] ..")

    parser = argparse.ArgumentParser(description="Robot VISION service")
    parser.add_argument('sketchfile', help='Sketch program file')
    parser.add_argument('--user', help='Flow-network username', default='guest')
    parser.add_argument('--password', help='Flow-network password', default='password')

    args = parser.parse_args()
    
    sat.setLogin(args.user, args.password)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setStartChanPubReqCallBack(onStartChanPub)
    sat.setStopChanPubReqCallBack(onStopChanPub)
    sat.setGrabDataCallBack(onDataGrabbed)
    sat.setRequestCallBack(onPromptServiceRequest)

    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:

        sat.setSpeedMonitorEnabled(False)
        print("[LOOP] ..")

        sat.addServiceChannel(promptChanName)

        sat.addStreamingChannel(Flow_T.FT_VIDEO_DATA, Variant_T.T_BYTEARRAY, inputChanName, "image/jpeg")
        sat.addStreamingChannel(Flow_T.FT_BLOB, Variant_T.T_STRING, outputChanName)
    
    return ok

def loop():
    global lastCmdHash

    if streamer is not None:
        if not active:
            return False

        if not streamer.text_queue.empty():
            streamerEx.acquire()

            generated = ""
            
            for new_text in streamer:
                sys.stdout.write(".")
                sys.stdout.flush()
                
                if len(new_text) > 0: 
                    if new_text.endswith(eot):
                        new_text = new_text[:-len(eot)]

                    if outputChan.isPublishingEnabled:
                        sat.publishString(outputChan.chanID, new_text)

                    generated += new_text

                sat.tick(False)

            print()

            streamerEx.release()

            sat.sendServiceResponse(promptChan.chanID, lastCmdHash, {"status" : True})
            lastCmdHash = None
            return sat.isConnected()

    sat.tick()
    return sat.isConnected()

def close():
    global closing

    if thread is not None:
        closing = True
        thread.join()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global thread
    global promptChan
    global inputChan
    global outputChan

    print("Channel ADDED: {}".format(ch.name))

    if ch.name == "{}.{}".format(sat._userName, promptChanName):
        promptChan = ch
        print("PromptService is READY: {}".format(promptChan.name))
    
    elif ch.name == "{}.{}".format(sat._userName, inputChanName):
        inputChan = ch
        print("InputChan is READY [auto-subscribe]: {}".format(inputChan.name))
        sat.subscribeChannel(inputChan.chanID)

    elif ch.name == "{}.{}".format(sat._userName, outputChanName):
        outputChan = ch
        print("OutputChan is READY: {}".format(outputChan.name))
        #thread = Thread(target=inferenceRunner)
        #thread.start()

        sat.setTickTimer(t, t * 5)

def onChannelRemoved(ch):
    print("Channel REMOVED: {}".format(ch.name))

def onStartChanPub(ch):
    print("Publish START required: {}".format(ch.name))

def onStopChanPub(ch):
    print("Publish STOP required: {}".format(ch.name))

def onDataGrabbed(chanID, data):
    global frame

    if ch.name == "{}.{}".format(sat._userName, inputChanName):
        np_array = np.frombuffer(data, np.uint8)
        frame = cv.imdecode(np_array, cv.IMREAD_COLOR)
        frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    
###############################################################################

terminateGeneration = False

class CustomStoppingCriteria(StoppingCriteria):
    def __init__(self):
        pass
    
    def __call__(self, input_ids, score, **kwargs):
        global terminateGeneration
        ok = terminateGeneration

        if terminateGeneration:
            terminateGeneration = False

        return ok

def onPromptServiceRequest(chanID, hash, cmdName, val):
    global promptPack
    global currentPrompt
    global lastCmdHash
    global terminateGeneration

    if promptChan.chanID == chanID:
        if cmdName == "CHAT":
            if currentPrompt is None:
                lastCmdHash = hash
                promptPack = val["value"]
                print("PromptPack RECEIVED [cmdName: {}; hash: {}]: {}".format(cmdName, lastCmdHash, promptPack))

            else:
                sat.sendServiceResponse(chanID, hash, {"status" : False})
                print("CANNOT accept the prompt -> another evaluation is going on background")

        elif cmdName == "STOP_GEN":
            if currentPrompt is None:
                sat.sendServiceResponse(chanID, hash, {"status" : False})
                print("CANNOT accept the generation stop request [STOP_GEN] -> generation process NOT found")
            else:
                sys.stdout.write("\n>>>\nTerminating generation process [STOP_GEN] ..")
                terminateGeneration = True
        
###############################################################################
# Inference thread

stoppingCriteria = StoppingCriteriaList([CustomStoppingCriteria()])

def inferenceRunner():
    global streamer
    global active
    global currentPrompt
    global promptPack
    global frame

    print("PyTorch Version:", torch.__version__)

    if torch.cuda.is_available():
        print("CUDA is available")

    else:
        print("CUDA is NOT available")
        exit(1)
    
    torch.cuda.init()
    #device = torch.cuda.device(0)
    
    model_id = "vikhyatk/moondream2"
    revision = "2024-04-02"

    print("Loading model: {} ..".format(model_id))

    model = AutoModelForCausalLM.from_pretrained(
        model_id,
        revision=revision,
        trust_remote_code=True,
        torch_dtype=torch.float16,
        resume_download=None,
        device_map='cuda'
    )

    #attn_implementation="flash_attention_2",
    tokenizer = AutoTokenizer.from_pretrained(
        model_id,
        revision=revision,
        resume_download=None,
        device_map='cuda'
    )

    streamer = TextIteratorStreamer(tokenizer, skip_prompt=True)

    print("READY")
    active = True

    while True:
        if closing:
            return

        if promptPack is None or frame is None:
            time.sleep(t)
            continue

        currentPrompt = promptPack["prompt"]
        promptPack = None

        enc_image = model.encode_image(frame)

        o = model.answer_question(
            enc_image,
            currentPrompt,
            tokenizer,
            streamer=streamer,
            stopping_criteria=stoppingCriteria
        )

        print(o)

        currentPrompt = None
        frame = None

###############################################################################

def main():
    inferenceRunner()

###############################################################################
